class UserMailer < ActionMailer::Base
  default from: 'lukasz.korol@unitedideas.pl'

  def recipe_notify(recipe)
    @recipe = recipe
    mail(to: 'lukas.korol@gmail.com', subject: '#{@recipe.name}')
  end
end
