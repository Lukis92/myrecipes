class Recipe < ActiveRecord::Base
  # Association
  belongs_to :chef
  has_many :likes, dependent: :destroy
  has_many :recipe_styles, dependent: :destroy
  has_many :styles, through: :recipe_styles
  has_many :recipe_ingredients, dependent: :destroy
  has_many :ingredients, through: :recipe_ingredients
  validates :chef_id, presence: true
  validates :name, presence: true, length: { minimum: 5, maximum: 100 }
  validates :description, presence: true, length: { minimum: 20, maximum: 500 }
  validates :summary, presence: true, length: { minimum: 10, maximum: 150 }
  # mount_uploader :picture, PictureUploader
  require 'carrierwave/orm/activerecord'
  # validate :picture_size

  def thumbs_up_total
    # self reprezentuje obiekt na rzecz którego wywołano metodę
    likes.where(like: true).size
  end

  def thumbs_down_total
    likes.where(like: false).size
  end

  private

  def picture_size
    if picture_size > 5.megabytes
      errors.add(:picture, 'should be less than 5MB')
    end
  end

  has_attached_file :picture, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
end
