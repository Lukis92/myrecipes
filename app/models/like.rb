class Like < ActiveRecord::Base
  belongs_to :chef
  belongs_to :recipe

  # możliwość zagłosowania tylko raz na przepis
  validates_uniqueness_of :chef, scope: :recipe
end
