class AddPictureToRecipes < ActiveRecord::Migration
  def up
    # add_column :recipes, :picture, :string
    add_attachment :recipes, :picture
  end

  def down
    remove_attachment :recipes, :picture
  end
end
